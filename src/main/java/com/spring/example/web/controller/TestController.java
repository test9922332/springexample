package com.spring.example.web.controller;

import com.spring.example.model.TestModel;
import com.spring.example.model.dto.EmployeeDto;
import com.spring.example.service.api.TestService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDateTime;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class TestController {

    private final TestService testService;

    @Value("${spring.test.message}")
    private String defaultMessage;

    @GetMapping("/test")
    public String getTestPage(Model model) {
        TestModel testModel = new TestModel();
        testModel.setMessage(defaultMessage);
        testModel.setDateTime(LocalDateTime.now().toString());
        model.addAttribute("testModel", testModel);
        return "test";
    }

    @GetMapping("/hello")
    public String getHelloPage(HttpServletRequest request, Model model) {
        model.addAttribute("name", "Mark");
        return "hello";
    }

    @GetMapping("/exception")
    public String throwException() {
        testService.testMethod();
        return "hello";
    }

    @GetMapping("/employee")
    public ModelAndView showForm() {
        return new ModelAndView("form", "employee", new EmployeeDto());
    }

    @PostMapping("/addEmployee")
    public String getForm(@ModelAttribute("employee") EmployeeDto employee, ModelMap model) {
        model.addAttribute("firstName", employee.getFirstName());
        model.addAttribute("lastName", employee.getLastName());
        return "result";
    }
}
