package com.spring.example.web;

import com.spring.example.service.exception.TestException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class TestExceptionHandler {

    @ExceptionHandler(TestException.class)
    public ModelAndView handleTestException(HttpServletRequest request, TestException ex) {
        var modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
