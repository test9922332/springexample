package com.spring.example.web.security;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ApplicationAuthenticationProvider implements AuthenticationProvider {

    @Value("${spring.security.username}")
    private String username;
    @Value("${spring.security.password}")
    private String password;

    @Override
    public Authentication authenticate(final Authentication authentication)
        throws AuthenticationException {
        var username = authentication.getName();
        var password = authentication.getCredentials().toString();

        // check user and password from DB
        // use encryption for password
        // add roles to user using GrantedAuthority (pattern - "ROLE_NAME", example "ROLE_ADMIN")
        // use BadCredentialsException if username not found or invalid
        if ("Mark".equals(username) && "Kram".equals(password)) {
            return new UsernamePasswordAuthenticationToken(username, password,
                List.of(new SimpleGrantedAuthority("ROLE_ADMIN")));
        } else {
            throw new BadCredentialsException("");
        }
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
