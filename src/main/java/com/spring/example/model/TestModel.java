package com.spring.example.model;

import lombok.Data;

@Data
public class TestModel {

    private String message;
    private String dateTime;

}