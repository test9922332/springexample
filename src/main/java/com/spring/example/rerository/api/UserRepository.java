package com.spring.example.rerository.api;

import com.spring.example.model.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByName(String name);

//    @Query("SELECT u FROM User u WHERE u.name = ?1")
    User find(String name);

    @Transactional
    @Modifying
//    @Query("UPDATE User set u.firstName="default" WHERE u.name = ?1")
    User update(String name);

}
