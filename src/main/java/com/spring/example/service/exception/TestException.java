package com.spring.example.service.exception;

public class TestException extends RuntimeException{

    public TestException(String message) {
        super(message);
    }
}
