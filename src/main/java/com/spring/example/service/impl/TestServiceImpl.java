package com.spring.example.service.impl;

import com.spring.example.service.api.TestService;
import com.spring.example.service.exception.TestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class TestServiceImpl implements TestService {

    @Override
    public void testMethod() {
        log.error("!!!!!!! WTF - some ERROR !!!!!!!!!");
        throw new TestException("MY CUSTOM ERROR!!!");
    }
}
