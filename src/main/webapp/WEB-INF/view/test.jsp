<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<sec:authorize access="isAuthenticated()">
    <html>
    <head>
        <%@ page isELIgnored="false" %>
        <meta charset="ISO-8859-1">
        <title>Spring 5 MVC - Test Example </title>
    </head>
    <body>
    <h2>${testModel.message}</h2>
    <h4>Server date time is : ${testModel.dateTime}</h4>
    </body>
    </html>
</sec:authorize>
