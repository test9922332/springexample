<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE HTML>
<sec:authorize access="hasRole('ADMIN')">
    <html>
    <head>
        <title>Sample Application</title>
    </head>
    <body>
    <h1>Hello, ${name}!</h1>
    </body>
    </html>
</sec:authorize>
